import { TimerConfigs } from "../utils/timerConfigs";

export const updateTimerConfigs = (newState: TimerConfigs) => {
    return {
        type: "UPDATE_TIMER_CONFIGS",
        payload: newState,
    };
};
