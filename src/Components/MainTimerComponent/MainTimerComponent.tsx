import {
    Button,
    Container,
    Grid,
    Paper,
    TextField,
    Typography,
} from "@material-ui/core";
import { TimePicker } from "@material-ui/pickers";
import React, { useState } from "react";
import moment from "moment";
import { connect, useDispatch } from "react-redux";
import { TimerConfigs } from "../../utils/timerConfigs";
import { updateTimerConfigs } from "../../actions/timeConfigs";

interface MainTimerComponentProps {
    timerConfigs: TimerConfigs;
}

function mapStateToProps(state: any) {
    const { timerConfigs } = state;
    return { timerConfigs };
}

const MainTimerComponent = (props: MainTimerComponentProps) => {
    const timerConfigs = props.timerConfigs;
    const dispatch = useDispatch();

    const handleTimerConfigChange = (key: keyof TimerConfigs) => (
        value: any
    ) => {
        const newTimerConfigs = { ...timerConfigs };
        newTimerConfigs[key] = value;
        dispatch(updateTimerConfigs(newTimerConfigs));
    };

    const setAlarm = () => {
        let timeToSetAt = moment(
            moment().format("YYYY-MM-DD") +
                " " +
                timerConfigs.time.format("HH:mm")
        );

        if (timeToSetAt.diff(moment()) < 0) {
            timeToSetAt = timeToSetAt.add(1, "days");
        }
        sendNotification(timeToSetAt);
    };

    const testNotificationNow = () => {
        const timeToSetAt = moment().add(5, "seconds");
        sendNotification(timeToSetAt);
    };

    const sendNotification = (timeToSetAt: moment.Moment) => {
        if (window.cordova) {
            //@ts-ignore
            window.cordova.plugins.notification.local.schedule({
                title: timerConfigs.title,
                text: timerConfigs.text,
                attachments: [
                    "https://cdn.craftbeer.com/wp-content/uploads/coffee2_hero.jpg",
                ],
                sound: "file://ring1.mp3",
                foreground: true,
                vibrate: true,
                trigger: {
                    at: timeToSetAt.toDate(),
                },
                actions: "yes-no",
            });
            alert("DONE");
        }
    };

    return (
        <div className="MainTimerComponent">
            <Container maxWidth="md">
                <br />
                <br />
                <Typography variant="h5">Sample Timer</Typography>
                <br />
                <br />
                <Paper>
                    <br />
                    <Container>
                        <Grid container>
                            <Grid item xs={12}>
                                <TimePicker
                                    value={timerConfigs.time}
                                    onChange={handleTimerConfigChange("time")}
                                />
                            </Grid>
                            {["title", "text", "imageUrl"].map((item) => {
                                const key = item as keyof TimerConfigs;
                                return (
                                    <Grid item key={item} xs={12}>
                                        <TextField
                                            value={timerConfigs[key]}
                                            onChange={(e) =>
                                                handleTimerConfigChange(key)(
                                                    e.target.value
                                                )
                                            }
                                        />
                                    </Grid>
                                );
                            })}
                        </Grid>

                        <Button onClick={setAlarm}>Set Alarm</Button>
                        <Button onClick={testNotificationNow}>
                            TestNotification Now
                        </Button>
                    </Container>
                    <br />
                </Paper>
            </Container>
        </div>
    );
};

export default connect(mapStateToProps)(MainTimerComponent);
