import moment from "moment";
import { TimerConfigs } from "../utils/timerConfigs";

const defaultTimerConfigs: TimerConfigs = {
    time: moment(),
    title: "Default Alarm",
    text: "Default Alarm Text",
    imageUrl: "https://www.emergency24.com/cp/images/toplevel_alarm.png",
};

export const timerConfigsReducer = (
    state = defaultTimerConfigs,
    action: any
): TimerConfigs => {
    switch (action.type) {
        case "UPDATE_TIMER_CONFIGS":
            return action.payload;
        default:
            return state;
    }
};
