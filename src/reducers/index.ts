import { combineReducers } from "redux";
import { timerConfigsReducer } from "./timerConfigs";

const rootReducer = combineReducers({
    timerConfigs: timerConfigsReducer,
});

export default rootReducer;
