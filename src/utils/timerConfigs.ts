import { Moment } from "moment";

export interface TimerConfigs {
    time: Moment;
    title?: string;
    text?: string;
    imageUrl?: string;
}
