import React from "react";
import logo from "./logo.svg";
import "./App.css";
import MainTimerComponent from "./Components/MainTimerComponent/MainTimerComponent";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import { createMuiTheme, CssBaseline, ThemeProvider } from "@material-ui/core";

const theme = createMuiTheme({
    palette: {
        type: "dark",
    },
});

function App() {
    return (
        <MuiPickersUtilsProvider utils={MomentUtils}>
            <ThemeProvider theme={theme}>
                <CssBaseline />

                <div className="App">
                    <MainTimerComponent />
                </div>
            </ThemeProvider>
        </MuiPickersUtilsProvider>
    );
}

export default App;
